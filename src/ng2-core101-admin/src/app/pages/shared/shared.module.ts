﻿import {NgModule, ModuleWithProviders} from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import {AdModule} from './ad.module';

@NgModule({
    imports: [CommonModule, RouterModule, AdModule],
    declarations: [],
    exports: [CommonModule, FormsModule, RouterModule, AdModule]
})

export class SharedModule{
    static forRoot() : ModuleWithProviders{
        return {
            ngModule: SharedModule
        }
    }
}