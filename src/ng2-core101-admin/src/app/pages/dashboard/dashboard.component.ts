import {Component, ViewEncapsulation} from '@angular/core';
import { Http, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import 'rxjs/add/observable/throw';
import 'rxjs/add/operator/map';
import 'rxjs/add/operator/catch';
import 'rxjs/Rx';

import {NG2_SMART_TABLE_DIRECTIVES, LocalDataSource} from 'ng2-smart-table';
import { LeagueListService } from '../shared/league-list/league-list.service';

@Component({
  selector: 'dashboard',
  encapsulation: ViewEncapsulation.None,
  styles: [require('./dashboard.scss')],
  template: require('./dashboard.html'),
  providers: [LeagueListService]
    ,  directives: [NG2_SMART_TABLE_DIRECTIVES],
})
export class Dashboard {

    query: string = '';

    settings = {
        add: {
            addButtonContent: '<i class="ion-ios-plus-outline"></i>',
            createButtonContent: '<i class="ion-checkmark"></i>',
            cancelButtonContent: '<i class="ion-close"></i>',
        },
        edit: {
            editButtonContent: '<i class="ion-edit"></i>',
            saveButtonContent: '<i class="ion-checkmark"></i>',
            cancelButtonContent: '<i class="ion-close"></i>',
        },
        delete: {
            deleteButtonContent: '<i class="ion-trash-a"></i>',
            confirmDelete: true
        },
        columns: {
            id: {
                title: 'ID',
                type: 'number'
            },
            firstName: {
                title: 'First Name',
                type: 'string'
            },
            lastName: {
                title: 'Last Name',
                type: 'string'
            },
            username: {
                title: 'Username',
                type: 'string'
            },
            email: {
                title: 'E-mail',
                type: 'string'
            },
            age: {
                title: 'Age',
                type: 'number'
            }
        }
    };

    source: LocalDataSource = new LocalDataSource();
    errorMessage:string;

    constructor(protected leagueService: LeagueListService) {
        //this.leagueService.getData().then((data) => {
        //    this.source.load(data);
        //});

        //let data = leagueService.getData().subscribe(res => {
        //    console.log("res");
        //    console.log(res);
        //});
        //this.source.load(data);

        leagueService.getData().subscribe( 
            data => {
                this.source.load(data);
            },
            error => this.errorMessage = <any>error
            );
        
    }

    onDeleteConfirm(event): void {
        if (window.confirm('Are you sure you want to delete?')) {
            event.confirm.resolve();
        } else {
            event.confirm.reject();
        }
    }
}
